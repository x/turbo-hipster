gear>=0.5.4,<1.0.0
python-swiftclient
python-keystoneclient

lockfile
python-daemon<2.0
extras
GitPython>=0.3.2.RC1
# NOTE(tonyb) Pillow isn't directly needed but it's pulled in via
# Collecting Pillow (from blockdiag>=1.5.0->sphinxcontrib-blockdiag>=0.5.5
# So cap as per global-requirements until https://launchpad.net/bugs/1501995
# is properly fixed
Pillow>=2.4.0,<3.0.0 # MIT
sphinxcontrib-programoutput
sphinxcontrib-seqdiag

mysql-python

requests
PyYAML>=3.1.0,<4.0.0

jenkins-job-builder
xmltodict
python-magic
